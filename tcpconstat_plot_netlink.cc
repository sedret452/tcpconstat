/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * tcpconstat_plot_netlink.cc: TCPconstat plot module, for
 * netlink-specific data retrieval.
 *
 * (part of TCPconstat)
 * Copyright	(C) 2014-2022 sedret452
 */

#include "tcpconstat_plot.hh"
#include "tcpconstat_plot_netlink.hh"

extern "C" {
#include <unistd.h>
}

using namespace tcpconstat_lib;

void
open_netlink_plot (tcpconstat_filter* match_filter, int count)
{
	const PLFLT pi = 3.1415926535897932384;
	const PLINT red    = 1;
	const PLINT yellow = 2;
	const PLINT green  = 3;
	const PLINT blue   = 4;
	const PLINT violet = 5;
	
	std::array<std::string, PEN> legend_names { "rtt", "rtt_var", "rcv_rtt", "" };
	std::array<PLINT, PEN> line_color { red, green, blue, yellow};
	std::array<PLINT, PEN> line_style { 1, 5, 1, 1};
	tcpconstatplot* plot = new tcpconstatplot (
									   std::string("tcpconstat RTT")
									   , legend_names
									   , line_color
									   , line_style
									   , yellow
									   , yellow
									  );

    struct timespec ts;
    ts.tv_sec  = 0;
    ts.tv_nsec = 0.5 * 10000;

	for (int n = 0; n < count; n++) {
        nanosleep( &ts, NULL ); // wait a little to simulate time elapsing
		rtt_vals rtts;
		netlink_get_rtt_vals (&rtts, match_filter);
		plot->plot_y1y2y3 (rtts.rtt, rtts.rtt_var, rtts.rcv_rtt);
    }
	delete plot;
}

void
open_netlink_plot (tcpconstat_filter* match_filter, int count, int interval_ms)
{
	const PLFLT pi = 3.1415926535897932384;
	const PLINT red    = 1;
	const PLINT yellow = 2;
	const PLINT green  = 3;
	const PLINT blue   = 4;
	const PLINT violet = 5;
	
	std::array<std::string, PEN> legend_names { "rtt", "rtt_var", "rcv_rtt", "" };
	std::array<PLINT, PEN> line_color { red, green, blue, yellow};
	std::array<PLINT, PEN> line_style { 1, 5, 1, 1};
	tcpconstatplot* plot = new tcpconstatplot (
									   std::string("tcpconstat RTT")
									   , legend_names
									   , line_color
									   , line_style
									   , yellow
									   , yellow
									  );

	useconds_t interval_us = interval_ms * 1000;
	for (int n = 0; n < count; n++ ) {
        usleep(interval_us);
		rtt_vals rtts;
		netlink_get_rtt_vals (&rtts, match_filter);
		plot->plot_y1y2y3 (rtts.rtt, rtts.rtt_var, rtts.rcv_rtt);
    }
	delete plot;
}
