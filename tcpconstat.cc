/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * tcpconstat.cc: TCPconstat, retrieve and plot TCP connection status infos.
 *
 *  Main part of the program and provides its CLI via getopt.
 *
 * (part of TCPconstat)
 * Copyright	(C) 2014-2022 sedret452
 */

extern "C" {
#include <unistd.h> // for getopt_long
#include <getopt.h>
}

#include <string>
#include <sstream>
#include <iostream>
#include <vector>

#include <cinttypes> // uint16_t, ...
extern "C" {
#include <arpa/inet.h>
}
#include <cstring> // memset

static const std::string prog_name    = "tcpconstat";
static const std::string prog_version = "0.5.1";

namespace tcpconstat_lib {
	extern "C" {
// :TODO: Either needs to be removed completely or restored
#include "_tcpconstat_proc.h"
#include "_tcpconstat_getsock.h"
		
#include "_tcpconstat_netlink.h"
	}
}

#include "tcpconstat_plot_netlink.hh"

#include <map>
#include <algorithm>

// As taken from Linux kernel <net/tcp_states.h>
const std::map<std::string, int> tcpf_states =
	{
		{"ESTABLISHED",  1 <<  1 }
		, {"SYN_SENT",   1 <<  2 }
		, {"SYN_RECV",   1 <<  3 }
		, {"FIN_WAIT1",  1 <<  4 }
		, {"FIN_WAIT2",  1 <<  5 }
		, {"TIME_WAIT",  1 <<  6 }
		, {"CLOSE",      1 <<  7 }
		, {"CLOSE_WAIT", 1 <<  8 }
		, {"LAST_ACK",   1 <<  9 }
		, {"LISTEN",     1 << 10 }
		, {"CLOSING",    1 << 11 }
	};

int dry_run = 0;

// Note: use Netlink always (/proc and getsockopt handling is
// removed)
int use_netlink = 1;
const int use_netlink_val = 256;
	
int src_port_flag = 0;
const int src_port_flag_val = 258;
	
int dst_port_flag = 0;
const int dst_port_flag_val = 259;
	
int src_host_flag = 0;
const int src_host_flag_val = 260;
	
int dst_host_flag = 0;
const int dst_host_flag_val = 261;

int tcp_state_flag = 0;
const int tcp_state_flag_val = 262;

int plot_flag = 0;
const int plot_flag_val = 263;

int count = 0;
int count_flag = 0;

int interval_ms = 0;
int interval_flag = 0;

std::string
usage_str (void)
{
	using std::endl;

	std::string all_tcp_states;
	for(const auto& kvp : tcpf_states) {
		all_tcp_states.append (" " + kvp.first);
	}

	std::stringstream usage;
	usage
		<< ::prog_name << ", version " << ::prog_version << endl
		<< "Usage: " << endl
		<< "       " << ::prog_name << " [-h | -v] [OPTIONS] [FILTER spec]" << endl
		<< endl
		<< "       OPTIONS:" << endl
		<< "         -h, --help           Show this help" << endl
		<< "         -v, --version        Show version" << endl
		<< "         --plot               Plot TCP information (requires --count)" << endl
		<< "         -c, --count=<N>      Show TCP information 'N'-times" << endl
		<< "         -i, --interval=<I>   Interval time for --count (requires --count)"  << endl
		<< "                                (Given value in milliseconds !)" << endl
		<< endl
		<< "       FILTER spec:" << endl
		<< "         --src-port=<port> | --dst-port=<port>" << endl
		<< "         --src-host=<IPv4> | --dst-host=<IPv4>" << endl
		<< endl
		<< "         --tcp-state=<state>   Use multiple times if needed" << endl
		<< endl
		<< "       TCP states:" << endl
		<< "         " << all_tcp_states
		<< endl;

	return usage.str();
}

std::string
version_str (void)
{
	using std::endl;

	std::stringstream version;
	version << ::prog_name << ", version " << ::prog_version << endl;

	return version.str();
}

int
get_tcpconstat_getsock (int sk)
{
	tcpconstat_lib::tcpconstat_getsock (sk);

	return 0;
}

bool
string_to_port (std::string port_str, uint16_t& port)
{
	std::istringstream opt (port_str);
	uint16_t tmp;
	opt >> tmp;

	if (tmp >= (1 << 16))
		return false;

	port = tmp;
	return true;
}

bool
string_to_ipv4_addr (std::string& ipv4_addr_str, struct in_addr* host)
{
	struct in_addr tmp;
	memset (&tmp, 0, sizeof (tmp));
	
	if (!inet_aton (ipv4_addr_str.c_str(), &tmp))
		return false;

	memcpy (host, &tmp, sizeof (tmp));
	return true;	
}

bool
parse_and_add_match_filter (tcpconstat_lib::tcpconstat_filter& f, int flag_val, std::string optarg)
{
	using std::cout;

	if (flag_val == src_port_flag_val) {
		uint16_t src_port = 0;

		if (string_to_port (optarg, src_port) == false) {
			cout << "Error: --src-port Invalid port number" << std::endl;
			return false;
		}
		
		f.src_port = src_port;
		f.mask |= tcpconstat_lib::filter_src_port;
		cout << "DEBUG: src-port: " << src_port << std::endl;
	}
	else if (flag_val == dst_port_flag_val) {
		uint16_t dst_port = 0;

		if (string_to_port (optarg, dst_port) == false) {
			cout << "Error: --dst-port Invalid port number" << std::endl;
			return false;
		}
		
		f.dst_port = dst_port;
		f.mask |= tcpconstat_lib::filter_dst_port;
		cout << "DEBUG: dst-port: " << dst_port << std::endl;
	}
	else if (flag_val == src_host_flag_val) {
		struct in_addr src_host;

		if (string_to_ipv4_addr (optarg, &src_host) == false) {
			cout << "Error: --src-host Invalid IPv4 given" << std::endl;
			return false;
		}

		memcpy (&(f.src_host), &src_host, (sizeof src_host));
		f.mask |= tcpconstat_lib::filter_src_host;
		cout << "DEBUG: src-host: " << inet_ntoa (src_host) << std::endl;
	}
	else if (flag_val == dst_host_flag_val) {
		struct in_addr dst_host;

		if (string_to_ipv4_addr (optarg, &dst_host) == false) {
			cout << "Error: --dst-host Invalid IPv4 given" << std::endl;
			return false;
		}

		memcpy (&(f.dst_host), &dst_host, (sizeof dst_host));
		f.mask |= tcpconstat_lib::filter_dst_host;
		cout << "DEBUG: dst-host: " << inet_ntoa (dst_host) << std::endl;
	}
	else if (flag_val == tcp_state_flag_val) {
		uint32_t tcp_state_mask = 0;

		std::string tcp_state;
		std::transform (optarg.begin(), optarg.end(), std::back_inserter(tcp_state), ::toupper);
		std::map<std::string, int>::const_iterator found = ::tcpf_states.find (tcp_state);

		if (found == tcpf_states.end()) {
			cout << "DEBUG: Unknown tcp-state: »" << optarg << "«" << std::endl;
			return false;
		}

		f.mask |= tcpconstat_lib::filter_tcp_state;
		f.tcp_state_mask |= found->second;

		cout << "DEBUG: state: " << tcp_state << ", 0x" << std::hex << f.tcp_state_mask << std::endl;
	}
	
	return true;
}

int
main (int argc, char** argv)
{
	using std::cout;
	using std::cerr;
	using std::endl;

	bool parse_error = false;
	// TODO Create parse_return value to shell environment

	tcpconstat_lib::tcpconstat_filter match_filter;
	memset (&match_filter, 0, sizeof (tcpconstat_lib::tcpconstat_filter));

	static struct option long_opts[] =
   		{
			{ "use-netlink", no_argument, &use_netlink, use_netlink_val }
			, { "dry-run",     no_argument, 0, 'n' }
			, { "help"   ,     no_argument, 0, 'h' }
			, { "version",     no_argument, 0, 'v' }
			, { "src-port",    required_argument, &src_port_flag, src_port_flag_val }
			, { "dst-port",    required_argument, &dst_port_flag, dst_port_flag_val }
			, { "src-host",    required_argument, &src_host_flag, src_host_flag_val }
			, { "dst-host",    required_argument, &dst_host_flag, dst_host_flag_val }
			, { "tcp-state",   required_argument, &tcp_state_flag, tcp_state_flag_val }
			, { "plot",        no_argument, &plot_flag, plot_flag_val }
			, { "count",       required_argument, 0, 'c'}
			, { "interval",    required_argument, 0, 'i'}
			, {0, 0, 0, 0}
		};

	int opt_index = 0;

	while (true) {
		int c = getopt_long (argc, argv, "c:hi:nv", long_opts, &opt_index);

		if (c == -1)
			break;

		switch (c) {
		case 0:
			{
				int flag_val = *long_opts[opt_index].flag;
				if (flag_val == src_port_flag || flag_val == dst_port_flag
					|| flag_val == src_host_flag || flag_val == dst_host_flag
					|| flag_val == tcp_state_flag)
					{
						if (parse_and_add_match_filter (match_filter, flag_val, optarg) == false)
							parse_error = false;
					}
			}		
			break;

		case 'c':
			{
				std::istringstream opt (optarg);
				opt >> count;
			}
			count_flag = 1;
			break;

		case 'n':
			dry_run = 1;
			break;

		case 'h':
			cout << usage_str();
			return 0;
			break;

		case 'i':
			{
				std::istringstream opt (optarg);
				opt >> interval_ms;
			}
			interval_flag = 1;
			break;
			
		case 'v':
			cout << version_str();
			break;

		case '?':
			cout << "DEBUG: ??" << endl;
			break;

		default:
			/* TODO
			 *
			 * There's a bug here 'optarg' doesn't seem to
			 * be set correctly at this point at all possible states!
			 */
			cerr << "Error: Unknown option"
				<< " »" << c << "«."
				<< endl
				<< usage_str();
				
			return 1;
		}
	}

	if (parse_error) {
		cout << ::prog_name << ": Usage error, parse error!"
			 << endl
			 << usage_str();
		return 1;
	}

	if (dry_run) {
		cout << "Dry run!" << endl;
		return 0;
	}

	// Check command line validity
	if (plot_flag) {
		if (! count_flag) {
			cout << ::prog_name << ": Usage error --plot requires --count!"
				 << endl
				 << usage_str();
			
			//TODO error handling
			return 0;
		}
	}
	if (interval_flag) {
		if (! count_flag) {
			cout << ::prog_name << ": Usage error --interval requires --count!"
				 << endl
				 << usage_str();
			
			//TODO error handling
			return 0;
		}
	}
	if (count_flag) {
		if (count == 0) {
			cout << ::prog_name << ": Usage error --count must be > 0!"
				 << endl
				 << usage_str();
		}
	}

	if (use_netlink) {
		cout << "DEBUG: Going to use netlink" << endl;

		if (plot_flag) {
			cout << "DEBUG: Going to plot returned values for count = "
				 << count << endl;

			if (interval_flag) {
				open_netlink_plot (&match_filter, count, interval_ms);
			}
			else {
				open_netlink_plot (&match_filter, count);
			}
		}
		else {
			if (interval_flag) {
				useconds_t useconds = interval_ms * 1000;

				for (int n = 1; n <= count; ++n) {
					usleep (useconds);
					open_netlink (&match_filter);
				}
			}
			else {
				for (int n = 1; n <= count; ++n) {
					open_netlink (&match_filter);
				}
			}
		}
		return 0;
	}
}
