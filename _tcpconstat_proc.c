/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * _tcpconstat_proc.c: (Internal) module for /proc-based retrieval of TCP
 * connection infos.
 *
 *   Retrieve TCP connection infos via /proc/net/netlink.
 *
 * (part of TCPconstat)
 * Copyright	(C) 2014-2022 sedret452
 */

#include "_tcpconstat_proc.h"
#include <stdio.h>

#define err_show_netlink_open_file 1

int
tcpconstat_proc (void)
{
	int ERR1 = 1;

	char buf[256];
	FILE* proc_netlink_file = NULL;
	const char* proc_netlink_file_name = "/proc/net/netlink";
	
	int protocol;
	int pid;
	int groups;
	int read_queue;
	int write_queue;
	int wq;
	int rc;
	unsigned long long sk;
	unsigned long long cb;

	proc_netlink_file = fopen (proc_netlink_file_name, "r");

	if (proc_netlink_file == NULL) {
		printf ("Error: Couldn't open netlink file\n");
		return err_show_netlink_open_file;
	}

	fgets (buf, sizeof(buf) - 1, proc_netlink_file);

	while (fgets (buf, sizeof (buf) - 1, proc_netlink_file)) {
		int read = sscanf (buf, "%llx %d %d %x %d %d %llx %d"
				, &sk, &protocol, &pid, &groups, &read_queue
				, &write_queue, &cb, &rc);

		// look only into TCP sockets
		printf ("socket: %llx\tread mem: %d  write mem: %d\n"
				, sk, read_queue, write_queue);			

	}

	fclose (proc_netlink_file);
	return 0;
}
