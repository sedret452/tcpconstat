/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * _tcpconstat_getsock.c: (Internal) module for SOL_TCP-based retrieval
 * of TCP connection infos.
 *
 *   Retrieve TCP connection infos via a `getsockopt(...)`.
 *
 * (part of TCPconstat)
 * Copyright	(C) 2014-2022 sedret452
 */

#include "_tcpconstat_getsock.h"

#include <netinet/tcp.h> /* SOL_TCP */
#include <stdio.h>

int
tcpconstat_getsock (int sk)
{
	struct tcp_info stats;
	int stats_len = sizeof (stats);
	int ret = getsockopt (sk, SOL_TCP, TCP_INFO, (void *)&stats, (socklen_t *) &stats_len);
	if (ret == 0) {
		printf ("Stats: \n");
	}
	else {
		printf ("Error: Printint stats: \n");
	}
}
