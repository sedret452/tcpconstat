/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * tcpconstat_log_tcp_info.c: TCP-specific data logging module.
 *
 *  Logger functions for TCP infos.
 *
 * (part of TCPconstat)
 * Copyright	(C) 2014-2022 sedret452
 */

#include "log_lib_levels.h"
#define  LOG_LEVEL_MOD  LOG_INFO
#include "log_lib.h"

#include <linux/inet_diag.h>
#include <netinet/tcp.h>

#include <inttypes.h>
#include <stdlib.h>
#include <arpa/inet.h>  // struct in_addr


/* TODO
 *
 * 1. For state information see '<net/tcpconstates.>'
 *
 * 2. Break down this output to dependent components, e.g.
 *
 *    a) State Information
 *
 *    b) Options from handshake (tcp_options,
 *       tcpi_snd_wscale, tcpi_rcv_wscale,
 *       ...)
 *
 *    c) Timerouts:  RTO, ATO, ...
 *
 *    d) ACKs, SACKs, LOST, retransmitted, facket
 *
 *    e) Times: last_data_sent, ...
 *
 *    f) Metrics: pmtu, rcv_ss_thresh, rtt,
 *       rttvar, snd_cwnd, advmss,,
 *       reordering?, rcv_rtt, rcv_space
 *    
 *    
 */
void
_log_info_tcp_info (struct tcp_info* info)
{
	uint32_t unacked = info->tcpi_unacked;
	uint32_t sacked  = info->tcpi_sacked;
	uint32_t lost    = info->tcpi_lost;
	uint32_t retrans = info->tcpi_retrans;
	uint32_t fackets = info->tcpi_fackets;
	uint32_t rto = info->tcpi_rto;
	uint32_t ato = info->tcpi_ato;
	uint8_t  snd_wscale = info->tcpi_snd_wscale; // 4bit value for scaling
	uint8_t  rcv_wscale = info->tcpi_rcv_wscale; // 4bit value for scaling

	// THIS makes sense ! (1448!), this seems to be measured "in bytes"
	uint32_t advmss =  info->tcpi_advmss; // bytes

	uint32_t snd_mss = info->tcpi_snd_mss;
	uint32_t rcv_mss = info->tcpi_snd_mss;

	// THIS makes sense, but only in the beginning
	// of a transmission.  If Receiving RTT is 48
	// milli seconds,  rcv_rtt shows ~48000
	// => rcv_rtt "in micro seconds"
	uint32_t rtt =  info->tcpi_rtt;  // micro seconds (usually)
	uint32_t rttvar =  info->tcpi_rttvar;
	uint32_t rcv_rtt = info->tcpi_rcv_rtt;
	uint32_t rcv_space = info->tcpi_rcv_space;

	log_info_cont (", unacked %lu, sacked %lu"
				   ", lost %u, retrans %u, fackets %u"
				   ", rto: %u, ato: %u"
				   ", rcv_wscale: %x, snd_wscale: %x"
				   ", advmss: %u, snd_mss: %u, rcv_mss: %u"
				   ", rtt: %u, rttvar: %u"
				   ", rcv_rtt: %u, rcv_space: %u"
				   "\n"
				   , unacked, sacked
				   , lost, retrans, fackets
				   , rto, ato
				   , rcv_wscale, snd_wscale
				   , advmss, snd_mss, rcv_mss
				   , rtt, rttvar
				   , rcv_rtt, rcv_space
				   );					
}


void
_log_inet_diag_msg (struct inet_diag_msg* msg)
{
	int lport = ntohs (msg->id.idiag_sport);
	int rport = ntohs (msg->id.idiag_dport);

	struct in_addr src_addr;
	src_addr.s_addr = (msg->id.idiag_src)[0];
	char* src_addr_str = 0;
	asprintf (&src_addr_str, "%s", inet_ntoa (src_addr));

	struct in_addr dst_addr;
	dst_addr.s_addr = (msg->id.idiag_dst)[0];
	char* dst_addr_str = 0;
	asprintf (&dst_addr_str, "%s", inet_ntoa (dst_addr));
					
	int read_queue  = ntohl (msg->idiag_rqueue);
	int write_queue = ntohl (msg->idiag_wqueue);
					
	int inode = msg->idiag_inode; // TODO

	log_info ("  src %s, dst %s"
			  ", lport %d, rport %d, read %lu, write %lu\n"
			  , src_addr_str, dst_addr_str
			  , lport, rport, read_queue, write_queue
			  );
	
	if (src_addr_str)
		free (src_addr_str);
	if (dst_addr_str)
		free (dst_addr_str);
}
