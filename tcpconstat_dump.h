/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * tcpconstat_dump.h: TCPconstat value dumping module, facility for dev
 * debugging.
 *
 *  Provide dev-specific dumping functions, for TCP and Netlink
 *  protocol data.
 *
 * (part of TCPconstat)
 * Copyright	(C) 2014-2022 sedret452
 */

#ifndef __TCPCONSTAT_DUMP_H_
#define __TCPCONSTAT_DUMP_H_

#include "log_lib_default.h"
#ifndef  LOG_LEVEL_MOD_TCPCONSTAT_DUMP
#define  LOG_LEVEL_MOD_TCPCONSTAT_DUMP  LOG_LEVEL_DEFAULT
#endif

#if LOG_LEVEL_MOD_TCPCONSTAT_DUMP >= LOG_DEBUG_DUMP

#include <inttypes.h>
#include <linux/netlink.h>
#include <linux/inet_diag.h>

void _dump_bytes (uint8_t* addr, uint32_t len, const char* line_prefix);
void _dump_nl_header (struct nlmsghdr* nl_header);
void _dump_inet_diag_msg (struct inet_diag_msg* diag_msg);
void _dump_inet_diag_req (struct inet_diag_req* diag_req);
#define dump_bytes(...) _dump_bytes(__VA_ARGS__)
#define dump_nl_header(...)  _dump_nl_header(__VA_ARGS__)
#define dump_inet_diag_msg(...) _dump_inet_diag_msg(__VA_ARGS__)
#define dump_inet_diag_req(...) _dump_inet_diag_req(__VA_ARGS__)

#else

#define dump_bytes(...)
#define dump_nl_header(...)
#define dump_inet_diag_msg(...)
#define dump_inet_diag_req(...)

#endif /* LOG_LEVEL_MOD_TCPCONSTAT_DUMP >= LOG_DEBUG_DUMP */

#endif /* __TCPCONSTAT_DUMP_H_ */
