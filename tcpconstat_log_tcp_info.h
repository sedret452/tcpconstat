/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * tcpconstat_log_tcp_info.h: TCP-specific data logging module.
 *
 *  Logger functions for TCP infos.
 *
 * (part of TCPconstat)
 * Copyright	(C) 2014-2022 sedret452
 */

#ifndef __TCPCONSTAT_INFO_TCP_INFO_H_
#define __TCPCONSTAT_INFO_TCP_INFO_H_

#include "log_lib_default.h"
#ifndef LOG_LEVEL_MOD_TCPCONSTAT_LOG
#define LOG_LEVEL_MOD_TCPCONSTAT_LOG  LOG_INFO_TCP_INFO
#endif

#define LOG_LEVEL_MOD_TCPCONSTAT_LOG  LOG_INFO_TCP_INFO
#if LOG_LEVEL_MOD_TCPCONSTAT_LOG >= LOG_INFO_TCP_INFO

#include <netinet/tcp.h>
#include <linux/inet_diag.h>
void _log_info_tcp_info (struct tcp_info* info);
#define log_info_tcp_info(...)  _log_info_tcp_info(__VA_ARGS__)

void _log_inet_diag_msg (struct inet_diag_msg* msg);
#define log_inet_diag_msg(...) _log_inet_diag_msg(__VA_ARGS__)

#else

#define log_info_tcp_info(...)
#define log_info_inet_msg(...)

#endif /* LOG_LEVEL >= LOG_INFO_TCP_INFO */

#endif /* __TCPCONSTAT_INFO_TCP_INFO_H_ */
