/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * _tcpconstat_netlink.c: (Internal) module for Netlink-based retrieval
 * of TCP connection infos.
 *
 * (part of TCPconstat)
 * Copyright	(C) 2014-2022 sedret452
 */

#include "_tcpconstat_netlink.h"

#include "log_lib_levels.h"
//#define  LOG_LEVEL_MOD  LOG_DEBUG
//#define  LOG_LEVEL_MOD  LOG_INFO_TCP_INFO
#define  LOG_LEVEL_MOD  LOG_DEBUG_DUMP
//#define  LOG_LEVEL_MOD  LOG_DEBUG
#include "tcpconstat_dump.h"
#include "tcpconstat_log_tcp_info.h"
#include "log_lib.h"

#include <asm/types.h>
#include <sys/socket.h>  // struct msghdr, ...
#include <linux/netlink.h>

#include <netinet/tcp.h> /* SOL_TCP */

#include <linux/inet_diag.h>

#include <stdlib.h> // malloc(), free()
#include <unistd.h> // close()

#include <inttypes.h> // uint32_t, ..

/* Instead of using getsockopt(..) it's possible to use a NETLINK
   socket to retrieve TCP information of (any) socket.

   For this NETLINK family NETLINK_INET_DIAG (or NETLINK_SOCK_DIAG
   which should be the same) has to be used.

   NETLINK_SOCK_DIAG / NETLINK_INET_DIAG provides following message
   types (nlh->nlmsg_type):

     TCPDIAG_GETSOCK

	 DCCPDIAG_GETSOCK

	 SOCK_DIAG_BY_FAMILY

   See linux source code for that
   (net/core/sock_diag.c::sock_diag_rcv_msg, kernel 3.6).

   The function finally used to process NETLINK_INET_DIAG messages is:

   net/ipv4/inet_diag.c::inet_diag_rcv_msg_compat(..)
   // IPv4 only?

   So, probably when using flag INET_DIAG_INFO the netlink payload
   will hold a 'struct tcpinfo'.  */

#include <stdio.h>
#include <string.h> // memset

#include <netlink/netlink.h>
#include <arpa/inet.h>
#include <asm/types.h>
#include <linux/types.h>
#include <sys/socket.h>

/* TODO

   Following defintions are similar to RTA_<..> defines from sockstat
   tool.  Maybe these are now defined now also for other extensions,
   in a newer version?  They should be integrated into
   <linux/netlink.h> !
*/
#include <linux/netlink.h> // nlmsghdr
#define NLA_OK(nla,len) ((len) >= (int)sizeof(struct nlattr) && \
			 (nla)->nla_len >= sizeof(struct nlattr) && \
			 (nla)->nla_len <= (len))
#define NLA_NEXT(nla,attrlen)	((attrlen) -= NLA_ALIGN((nla)->nla_len), \
				 (struct nlattr*)(((char*)(nla)) + NLA_ALIGN((nla)->nla_len)))
#define NLA_LENGTH(len)	(NLA_ALIGN(sizeof(struct nlattr)) + (len))
#define NLA_SPACE(len)	NLA_ALIGN(NLA_LENGTH(len))

#define NLA_DATA(nla)   ((void*)(((char*)(nla)) + NLA_LENGTH(0)))

#include <linux/inet_diag.h> // inet_diag_req
#include <netinet/tcp.h>
#include <errno.h>

// Control struture, stores control information globally
// e.g. file descriptor.
typedef struct _control {
	int fd;
	struct sockaddr_nl sa;
} Control;
Control* control = NULL;

Control*
open_netlink_connection (void)
{
	Control* result = NULL;
	result = (Control*) malloc (sizeof (Control));

	if (result == NULL) {
		printf ("Error: memory allocation\n");
		return NULL;
	}

	// initialization
	memset (result, 0, sizeof (Control));
	result->fd = -1;

	int fd = socket (AF_NETLINK, SOCK_RAW, NETLINK_INET_DIAG);
	if (fd < 0) {
		perror ("create_control\n");
		free (result);
		return NULL;
	}
	else {
		result->fd = fd;
	}
}

typedef struct _netlink_request {
	struct nlmsghdr nlh;
	struct inet_diag_req r;
} Netlink_Request;

Netlink_Request*
create_inet_diag_request (tcpconstat_filter* f)
{
	Netlink_Request* req = NULL;
	req = (Netlink_Request*) malloc (sizeof (Netlink_Request));
	if (req == NULL) {
		printf ("Error: memory allocation\n");
		return NULL;
	}
	// Initialization
	memset (req, 0, sizeof (Netlink_Request));

	req->nlh.nlmsg_len  = sizeof (Netlink_Request);
	req->nlh.nlmsg_type = TCPDIAG_GETSOCK;
	req->nlh.nlmsg_pid = 0;
	req->nlh.nlmsg_seq = 123456;

	// AF_INET, AF_INET6
	req->r.idiag_family = AF_INET;

	// Activating extra TCP information
	req->r.idiag_ext |= (1 << (INET_DIAG_INFO-1));

	
	// Using request filter (no pattern filter!)
	//
	// Filter by:
	// a) TCP states (multiple ones, using states mask)
	//
	// b) Endpoint addresses (source/destination port or IPv4)
	//
	if (! f->mask) {
		req->nlh.nlmsg_flags = NLM_F_ROOT | NLM_F_MATCH | NLM_F_REQUEST;
		// TODO
		//req->r.idiag_states = 0xFF;
	}
	else {
		req->nlh.nlmsg_flags = NLM_F_MATCH | NLM_F_REQUEST;
		log_debug ("TODO: using request filter (mask 0x%04x) for:", f->mask);
		if (f->mask & filter_src_port) {
			req->r.id.idiag_sport = htons (f->src_port);
			log_debug_cont (" sport = %d", f->src_port);
		}
		if (f->mask & filter_dst_port) {
			req->r.id.idiag_dport = htons (f->dst_port);
			log_debug_cont (" dport = %d", f->dst_port);
		}
		if (f->mask & filter_src_host) {
			(req->r.id.idiag_src)[0] = f->src_host.s_addr;
			log_debug_cont (" src = %d", inet_ntoa(f->src_host));
		}
 		if (f->mask & filter_dst_host) {
			(req->r.id.idiag_dst)[0] = f->dst_host.s_addr;
			log_debug_cont (" dst = %d", inet_ntoa(f->dst_host));
		}

		/* TODO
		 *
		 * 'idiag_states' are configured through TCPF_<state> enum, these
		 * are defined in a Linux kernel internal header:
		 *
		 *   <net/tcp_states>
		 *
		 * Selecting all possible states by: 0xF (Nope, this excludes TCPF_LISTEN!)
		 *
		 * 'idiag_states' is a state mask of state flags!
		 */
		if (f->mask & filter_tcp_state) {
			req->r.idiag_states = f->tcp_state_mask;
			log_debug_cont (" state = 0x%08x", f->tcp_state_mask);
		}
		else {
			req->r.idiag_states = 0xF;
		}
		
		log_debug_cont ("\n");
	}
	
	dump_inet_diag_req (&(req->r));	
	return req;
}

Control*
send_inet_diag_request (tcpconstat_filter* f)
{
	Control* control = open_netlink_connection ();
	if (control == NULL) {
		return NULL;
	}

	Netlink_Request* req = create_inet_diag_request (f);
	if (req == NULL) {
		free (control);
		return NULL;
	}

	control->sa.nl_family = AF_NETLINK;

	struct msghdr msg;
	struct iovec iov[3]; // TODO
	iov[0] = (struct iovec) {
		.iov_base = req,
		.iov_len = sizeof (Netlink_Request)
	};
	msg = (struct msghdr) {
		.msg_name = (void*) &(control->sa),
		.msg_namelen = sizeof (struct sockaddr_nl),
		.msg_iov = iov,
		.msg_iovlen = 1
	};

	if (sendmsg (control->fd, &msg, 0) < 0) {
		log_debug ("Error sending msg\n");
		close (control->fd);
		free (control);
		free (req);		
		return NULL;
	}

	free (req);
	return control;
}

int
parse_netlink_msg (struct nlmsghdr* nh, int len)
{
	if (!NLMSG_OK(nh, len)) {
		log_debug ("Error, ! NLMSG_OK\n");
		perror ("Dunno\n");
		return -1;
	}

	while (NLMSG_OK (nh, len)) {
		log_debug ("New NL msg:\n");
		log_debug ("  nlmsg_type: 0x%02X\n", nh->nlmsg_type);
		dump_nl_header (nh);

		if (nh->nlmsg_type == NLMSG_ERROR) {
			struct nlmsgerr* err = (struct nlmsgerr*) NLMSG_DATA (nh);
			if (nh->nlmsg_len < NLMSG_LENGTH (sizeof (struct nlmsgerr))) {
				fprintf(stderr, "ERROR truncated\n");
			}
			else {
				errno = -(err->error);
				perror ("tcpdiag error: ");
				return -1;
			}
		}
		else if (nh->nlmsg_type == TCPDIAG_GETSOCK) {
			size_t payload_len = NLMSG_PAYLOAD (nh, len);
					
			struct inet_diag_msg* payload;
			payload = (struct inet_diag_msg*) NLMSG_DATA (nh);

			dump_inet_diag_msg (payload);
			log_inet_diag_msg  (payload);
					
			// Print inet_diag information
			int l = nh->nlmsg_len - NLMSG_LENGTH(sizeof(*payload));
			struct nlattr* ext = (struct nlattr*)(payload + 1);
			while (NLA_OK(ext,l)) {
						

				if (ext->nla_type != INET_DIAG_MAX) { // handling for all extensions
					log_debug ("== INET_DIAG Extension ==\n");
					log_debug ("  nlattr: type: 0x%02X  len: 0x%02X\n", ext->nla_type, ext->nla_len);
					log_debug ("  nlattr flags: nested %c, network byte order %c\n"
							   , ( (ext->nla_type & NLA_F_NESTED) ? 'N' : '-')
							   , ( (ext->nla_type & NLA_F_NET_BYTEORDER) ? 'O' : '-')
							   ); // I don't think nested flag
					              // and net_byteorder flag
					              // are used in INET_DIAG at
					              // all, but checking them
					              // 'doesn't hurt'(TM).
					dump_bytes ((uint8_t*) ext, ext->nla_len + 3, "  ");
				}
						
				if (ext->nla_type == INET_DIAG_INFO) {
					log_info ("-- INET_DIAG_INFO extension --\n");
					struct tcp_info* info = NLA_DATA(ext);
					log_info_tcp_info (info);
							
				}
				else if (ext->nla_type == INET_DIAG_SHUTDOWN) {
					log_info ("-- INET_DIAG_SHUTDOWN extension --\n");
					log_info ("Nothing to be done here!\n");
				}
				else {
					// TODO
				}
				ext = NLA_NEXT(ext,l);

			}

			if (nh->nlmsg_type == NLMSG_DONE) {
				log_info ("NLMSG_DONE\n");
				return NLMSG_DONE; // into while loop!
			}
		}
		else {
			log_debug ("New NL msg:\n");
			log_debug ("\tSkipping message type: 0x%02X, len: %d\n", nh->nlmsg_type, len);
			if (nh->nlmsg_type == NLMSG_DONE) {
				return 0; // into while loop!
			}
		}

		nh = NLMSG_NEXT (nh, len);
	}
}

int
parse_netlink_msg_get_rtt_vals (struct nlmsghdr* nh, int len, rtt_vals* rtts)
{
	if (!NLMSG_OK(nh, len)) {
		log_debug ("Error, ! NLMSG_OK\n");
		perror ("Dunno\n");
		return -1;
	}

	while (NLMSG_OK (nh, len)) {
		log_debug ("New NL msg:\n");
		log_debug ("  nlmsg_type: 0x%02X\n", nh->nlmsg_type);
		dump_nl_header (nh);

		if (nh->nlmsg_type == NLMSG_ERROR) {
			struct nlmsgerr* err = (struct nlmsgerr*) NLMSG_DATA (nh);
			if (nh->nlmsg_len < NLMSG_LENGTH (sizeof (struct nlmsgerr))) {
				fprintf(stderr, "ERROR truncated\n");
			}
			else {
				errno = -(err->error);
				perror ("tcpdiag error: ");
				return -1;
			}
		}
		else if (nh->nlmsg_type == TCPDIAG_GETSOCK) {
			size_t payload_len = NLMSG_PAYLOAD (nh, len);
					
			struct inet_diag_msg* payload;
			payload = (struct inet_diag_msg*) NLMSG_DATA (nh);

			// Print inet_diag information
			int l = nh->nlmsg_len - NLMSG_LENGTH(sizeof(*payload));
			struct nlattr* ext = (struct nlattr*)(payload + 1);
			while (NLA_OK(ext,l)) {
				if (ext->nla_type != INET_DIAG_MAX) { // handling for all extensions
					log_debug ("== INET_DIAG Extension ==\n");
					log_debug ("  nlattr: type: 0x%02X  len: 0x%02X\n", ext->nla_type, ext->nla_len);
					log_debug ("  nlattr flags: nested %c, network byte order %c\n"
							   , ( (ext->nla_type & NLA_F_NESTED) ? 'N' : '-')
							   , ( (ext->nla_type & NLA_F_NET_BYTEORDER) ? 'O' : '-')
							   ); // I don't think nested flag
					              // and net_byteorder flag
					              // are used in INET_DIAG at
					              // all, but checking them
					              // 'doesn't hurt'.
					dump_bytes ((uint8_t*) ext, ext->nla_len + 3, "  ");
				}
				if (ext->nla_type == INET_DIAG_INFO) {
					log_info ("-- INET_DIAG_INFO extension --\n");
					struct tcp_info* info = NLA_DATA(ext);
					rtts->rtt = info->tcpi_rtt;
					rtts->rtt_var = info->tcpi_rttvar;
					rtts->rcv_rtt = info->tcpi_rcv_rtt;
				}
				ext = NLA_NEXT(ext,l);

			}

			if (nh->nlmsg_type == NLMSG_DONE) {
				log_info ("NLMSG_DONE\n");
				return NLMSG_DONE; // into while loop!
			}
		}
		else {
			log_debug ("New NL msg:\n");
			log_debug ("\tSkipping message type: 0x%02X, len: %d\n", nh->nlmsg_type, len);
			if (nh->nlmsg_type == NLMSG_DONE) {
				return 0; // into while loop!
			}
		}

		nh = NLMSG_NEXT (nh, len);
	}
}

int
open_netlink (tcpconstat_filter* f)
{
	Control* control = send_inet_diag_request(f);

	const unsigned int MAXMSG = 8192;
	int len = 0;
	char buf [MAXMSG];
	memset (buf, 0, sizeof (buf));
	struct iovec iov[3];
	iov[0] = (struct iovec) {
		.iov_base = buf,
		.iov_len  = sizeof (buf)
	};
	struct sockaddr_nl nladdr;
	memset (&nladdr, 0, sizeof (nladdr));
	nladdr.nl_family = AF_NETLINK;

	struct msghdr msg;  // 'struct msghdr' defined in <sys/socket.h>

	while (1) {
		struct nlmsghdr* nh = NULL;
		
		msg = (struct msghdr) {
			(void*) &nladdr,
			sizeof (nladdr),
			iov,
			1,
			NULL,
			0,
			0
		};

		len = recvmsg (control->fd, &msg, 0);
		if (len < 0) {
			perror ("reading netlink");
			// TODO FREE HERE
			return -1;			
		}
		else if (len == 0) {
			log_warn ("Netlink socket orderly shutdown! (by whom?)\n");
			// TODO FREE HERE			
			return 0;
		}
		printf ("Read bytes: %d\n", len);

		nh = (struct nlmsghdr*) buf;

		int ret = parse_netlink_msg (nh, len);
		if (!ret) {
			close (control->fd);
			free (control);
			return ret;
		}
	} // while
}

int
netlink_get_rtt_vals (rtt_vals* rtts, tcpconstat_filter* f)
{
	memset (rtts, 0, sizeof (rtt_vals));	
		
	Control* control = send_inet_diag_request(f);

	const unsigned int MAXMSG = 8192;
	int len = 0;
	char buf [MAXMSG];
	memset (buf, 0, sizeof (buf));
	struct iovec iov[3];
	iov[0] = (struct iovec) {
		.iov_base = buf,
		.iov_len  = sizeof (buf)
	};
	struct sockaddr_nl nladdr;
	memset (&nladdr, 0, sizeof (nladdr));
	nladdr.nl_family = AF_NETLINK;

	struct msghdr msg;  // 'struct msghdr' defined in <sys/socket.h>

	while (1) {
		struct nlmsghdr* nh = NULL;
		
		msg = (struct msghdr) {
			(void*) &nladdr,
			sizeof (nladdr),
			iov,
			1,
			NULL,
			0,
			0
		};

		len = recvmsg (control->fd, &msg, 0);
		if (len < 0) {
			perror ("reading netlink");
			// TODO FREE HERE
			return -1;			
		}
		else if (len == 0) {
			log_warn ("Netlink socket orderly shutdown! (by whom?)\n");
			// TODO FREE HERE			
			return 0;
		}
		printf ("Read bytes: %d\n", len);

		nh = (struct nlmsghdr*) buf;
		
		int ret = parse_netlink_msg_get_rtt_vals (nh, len, rtts);
		if (!ret) {
			close (control->fd);
			free (control);
			return ret;
		}
	} // while
}
