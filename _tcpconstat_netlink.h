/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * _tcpconstat_netlink.h: (Internal) module for Netlink-based retrieval
 * of TCP connection infos.
 *
 * (part of TCPconstat)
 * Copyright	(C) 2014-2022 sedret452
 */

#ifndef __TCPCONSTAT_NETLINK_H_
#define __TCPCONSTAT_NETLINK_H_

#include <inttypes.h>
#include <arpa/inet.h>

const uint8_t filter_src_port = (1 << 0);
const uint8_t filter_dst_port = (1 << 1);
const uint8_t filter_src_host = (1 << 2);
const uint8_t filter_dst_host = (1 << 3);
const uint8_t filter_tcp_state = (1 << 4);

struct _tcpconstat_filter {
	int mask;
	uint16_t src_port;
	uint16_t dst_port;
	struct in_addr src_host;
	struct in_addr dst_host;
	uint32_t tcp_state_mask;
};
typedef struct _tcpconstat_filter tcpconstat_filter;

int open_netlink (tcpconstat_filter*);


typedef struct {
	uint32_t rtt;
	uint32_t rtt_var;
	uint32_t rcv_rtt;
} rtt_vals;
int netlink_get_rtt_vals (rtt_vals* rtts, tcpconstat_filter*);

#endif /* __TCPCONSTAT_NETLINK_H_ */
