/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * tcpconstat_plot_test.cc: Test application for PLplot.
 *
 * (part of TCPconstat)
 * Copyright	(C) 2014-2022 sedret452
 */

#include "tcpconstat_plot.hh"
#include <cmath>

int
main (int argc, const char** argv)
{
	const PLFLT pi = 3.1415926535897932384;
	const PLINT red    = 1;
	const PLINT yellow = 2;
	const PLINT green  = 3;
	const PLINT blue   = 4;
	const PLINT violet = 5;
	
	std::array<std::string, PEN> legend_names { "sin", "sin", "", "" };
	std::array<PLINT, PEN> line_color { red, green, blue, yellow};
	std::array<PLINT, PEN> line_style { 1, 5, 1, 1};
	tcpconstatplot* plot = new tcpconstatplot (
									   std::string("Sinus stripchart")
									   , legend_names
									   , line_color
									   , line_style
									   , yellow
									   , yellow
									  );

    struct timespec ts;
    ts.tv_sec  = 0;
    ts.tv_nsec = 0.5 * 10000000;

    for (int n = 0; n < 10000; n++ ) {
        nanosleep( &ts, NULL ); // wait a little to simulate time elapsing
		plot->plot_y1( sin ( (n * 0.1) * pi / 10));
    }

	delete plot;
	return 0;
}
