/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * log_lib_levels.h: Definition of log levels.
 *
 * (part of TCPconstat)
 * Copyright	(C) 2014-2022 sedret452
 */

#ifndef __LOG_LIB_LEVELS_H_
#define __LOG_LIB_LEVELS_H_

/* Specialized levels */
#define LOG_DEBUG_DUMP 21
#define LOG_INFO_TCP_INFO 11

/* Default levels */
#define LOG_DEBUG 20
#define LOG_INFO  10
#define LOG_WARN  1

#endif /* __LOG_LIB_LEVELS_H_ */
