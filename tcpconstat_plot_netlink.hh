/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * tcpconstat_plot_netlink.hh: TCPconstat plot module, for
 * netlink-specific data retrieval.
 *
 * (part of TCPconstat)
 * Copyright	(C) 2014-2022 sedret452
 */

#ifndef __TCPCONSTAT_PLOT_NETLINK_HH_
#define __TCPCONSTAT_PLOT_NETLINK_HH_


namespace tcpconstat_lib {
	extern "C" {
#include "_tcpconstat_netlink.h"
	}
}

void
open_netlink_plot (tcpconstat_lib::tcpconstat_filter* match_filter, int count);

void
open_netlink_plot (tcpconstat_lib::tcpconstat_filter* match_filter, int count, int interval_ms);


#endif // __TCPCONSTAT_PLOT_NETLINK_HH_
