/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * tcpconstat_plot.hh: TCPconstat plot module.
 *
 * (part of TCPconstat)
 * Copyright	(C) 2014-2022 sedret452
 */

#ifndef __TCPCONSTAT_PLOT_HH_
#define __TCPCONSTAT_PLOT_HH_

#include <array>
#include <string>
#include <plplot/plstream.h>

// DAFQ, Pen is 4 always for this kind of stripchart!  This is not
// configurable here, needs to be done directly in plplot code!
#define PEN 4

class tcpconstatplot {
private:
	plstream *pls;
	std::string plot_name;
	std::array<std::string, PEN> legend_names;
	std::array<PLINT, PEN> line_color;
	std::array<PLINT, PEN> line_style;

	PLINT box_color;   // coloring of plotting box
	PLINT label_color; // coloring of labels

	PLFLT legend_position[2];

	PLFLT t;
	PLFLT dt;

	PLINT id1;

public:
	tcpconstatplot (std::string plot_name
				 , std::array<std::string, PEN> legend_names
				 , std::array<PLINT, PEN> line_color
				 , std::array<PLINT, PEN> line_style
				 , PLINT box_color
				 , PLINT label_color
				 );

	void set_plot_name (std::string plot_name)
	{
		plot_name = plot_name;
	}
	
	void set_legend_names (std::array<std::string, PEN> legend_names)
	{
		legend_names = legend_names;
	}
	
	void set_line_color (std::array<std::string, PEN> line_color)
	{
		line_color = line_color;
	}
	
	void set_line_style (std::array<std::string, PEN> line_style)
	{
		line_style = line_style;
	}

	void plot_y1(PLFLT y1);
	void plot_y1y2y3 (PLFLT y1, PLFLT y2, PLFLT y3);
	~tcpconstatplot();

	static PLINT pl_errcode;
	static char  errmsg[160];
};

#endif // __TCPCONSTAT_PLOT_HH_
