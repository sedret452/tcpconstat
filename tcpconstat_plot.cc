/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * tcpconstat_plot.cc: TCPconstat plot module.
 *
 * (part of TCPconstat)
 * Copyright	(C) 2014-2022 sedret452
 */

#include "tcpconstat_plot.hh"
#include <ctime> // nanosleep()
#include <iostream>

PLINT tcpconstatplot::pl_errcode = 0;
char  tcpconstatplot::errmsg[160] = "";

tcpconstatplot::~tcpconstatplot ()
{
	pls->stripd (id1);
	delete pls;
}

tcpconstatplot::tcpconstatplot (std::string plot_name
						  , std::array<std::string, PEN> legend_names
						  , std::array<PLINT, PEN> line_color
						  , std::array<PLINT, PEN> line_style
						  , PLINT box_color
						  , PLINT label_color
						  )
	: plot_name(plot_name)
	, legend_names (legend_names)
	, line_color (line_color)
	, line_style (line_style)
	, box_color (box_color)
	, label_color (label_color)
	, legend_position {0.0, 0.55}
	, dt (0.1)
	, t (0.0)
{
	PLINT           n, nsteps = 10000;
    bool            autoy, acc;
    PLFLT           y1, y2, y3, y4, ymin, ymax, xlab, ylab;
    PLFLT           t, tmin, tmax, tjump, dt, noise;
	
    pls = new plstream();
	pls->sdev ("qt");

    // If db is used the plot is much more smooth. However, because of the
    // async X behaviour, one does not have a real-time scripcharter.
    //pls->setopt("db", "");
    //pls->setopt("np", "");
    // Specify some reasonable defaults for ymin and ymax
    // The plot will grow automatically if needed (but not shrink)

    ymin = 0.5 * 1000000;
    ymax = 0.501 * 1000000;

    // Specify initial tmin and tmax -- this determines length of window.
    // Also specify maximum jump in t
    // This can accomodate adaptive timesteps

    tmin  = 0.;
    tmax  = 20.;
    tjump = 0.8;      // percentage of plot to jump

    // Axes options same as plbox.
    // Only automatic tick generation and label placement allowed
    // Eventually I'll make this fancier

    autoy = true;             // autoscale y
    acc   = false;            // don't scrip, accumulate

    // Initialize PLplot.
    pls->init();

    pls->adv( 0 ); // TODO ??
    pls->vsta();   // TODO ??

    // Register our error variables with PLplot
    // From here on, we're handling all errors here
    pls->sError( &pl_errcode, errmsg );

	const char* legline[PEN] = { legend_names[0].c_str(), legend_names[1].c_str()
							 , legend_names[2].c_str(), legend_names[3].c_str() };
	
    pls->stripc( &id1, "bcnst", "bcnstv"
				 , tmin, tmax, tjump, ymin, ymax
				 , legend_position[0], legend_position[1]
				 , autoy, acc
				 , box_color, label_color
				 , line_color.data(), line_style.data()
				 , legline
				 , "t", "val", plot_name.c_str() );

    if ( pl_errcode )
    {
		std::cout << errmsg << std::endl;
        delete pls;
        exit( 1 );
    }

    // Let plplot handle errors from here on
    pls->sError( NULL, NULL );

	y1 = y2 = 0.0;
    dt = 0.1;
}


void
tcpconstatplot::plot_y1 (PLFLT y1)
{
	t += dt;
	pls->stripa (id1, 1, t, y1);
}

void
tcpconstatplot::plot_y1y2y3 (PLFLT y1, PLFLT y2, PLFLT y3)
{
t += dt;

pls->stripa (id1, 2, t, y3);
}
