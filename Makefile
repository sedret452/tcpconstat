# SPDX-License-Identifier: GPL-2.0-only

# Makefile of TCPconstat

CC=gcc-10
CFLAGS=-ggdb -O0 $(shell pkg-config --cflags plplot-c++ libnl-3.0)

CXX=g++-10
CXXFLAGS=-std=c++11 -ggdb -O0 $(shell pkg-config --cflags plplot-c++ libnl-3.0)

LDLIBS=$(shell pkg-config --libs plplot-c++ libnl-3.0)

TCPCONSTAT_LIBS = _tcpconstat_netlink _tcpconstat_proc _tcpconstat_getsock tcpconstat_dump tcpconstat_log_tcp_info
TCPCONSTAT_LIBS_OBJS =  $(foreach lib, $(TCPCONSTAT_LIBS), $(addprefix $(lib), .o))

TCPCONSTAT_LIBS_CXX = tcpconstat_plot tcpconstat_plot_netlink
TCPCONSTAT_LIBS_CXX_OBJS =  $(foreach lib, $(TCPCONSTAT_LIBS_CXX), $(addprefix $(lib), .o))

LOG_LIB = log_lib.h log_lib_levels.h log_lib_default.h

CODE_FILES = tcpconstat.cc $(foreach lib, $(TCPCONSTAT_LIBS),$(addprefix $(lib), .c .h)) $(foreach lib, $(TCPCONSTAT_LIBS_CXX),$(addprefix $(lib), .cc .hh)) $(LOG_LIB)
OBJ_FILES = tcpconstat $(TCPCONSTAT_LIBS_OBJS) $(TCPCONSTAT_LIBS_CXX_OBJS) tcpconstat_plot_test

tcpconstat: tcpconstat.cc $(TCPCONSTAT_LIBS_OBJS) $(TCPCONSTAT_LIBS_CXX_OBJS)
	$(CXX) $(CXXFLAGS)  -o tcpconstat $^ $(LDLIBS)

# All _tcpconstat_<...> libraries
_%.o: _%.c _%.h $(LOG_LIB) tcpconstat_dump.o tcpconstat_log_tcp_info.o
	$(CC) $(CFLAGS) -c $<

%.c.pre: %.c %.h $(LOG_LIB)
	$(CC) $(CFLAGS) -E -o $@ $<

tcpconstat_dump.o: tcpconstat_dump.c tcpconstat_dump.h $(LOG_LIB)
	$(CC) $(CFLAGS) -c $<

tcpconstat_log_tcp_info.o: tcpconstat_log_tcp_info.c tcpconstat_log_tcp_info.h $(LOG_LIB)
	$(CC) $(CFLAGS) -c $<

tcpconstat_plot.o: tcpconstat_plot.cc
	$(CXX) $(CXXFLAGS) -c $<

tcpconstat_plot_test: tcpconstat_plot_test.cc tcpconstat_plot.o
	$(CXX) $(CXXFLAGS) -o $@ $^ $(LDLIBS)

tcpconstat_plot_netlink.o: tcpconstat_plot_netlink.cc tcpconstat_plot_netlink.hh tcpconstat_plot.o _tcpconstat_netlink.o
	$(CXX) $(CXXFLAGS) -c $(filter-out %.o,$^)


.PHONY: clean clean-expand-files cscope cscope-file-db clean-cscope clean-cscope-file-db

cscope: $(CODE_FILES)
	cscope -bq

cscope-file-db:
	@echo "Creating cscope.files"
	@{ for f in ${CODE_FILES} ; do echo $$f; done } | tee cscope.files

clean-expand-files:
	@for f in $(wildcard *.expand) ; do echo "Deleting $$f" ; rm $$f ; done

clean-cscope-file-db:
	@for f in $(wildcard cscope.files) ; do rm $$f ; done

clean-cscope:
	@for f in  $(wildcard $(addprefix cscope,.in.out .po.out .out)) ; do rm $$f ; done
	@#echo rm $(wildcard $(addsuffix .out, $(addprefix cscope,.in .po '' )))

clean:
	@echo "Cleanup"
	@for f in $(wildcard $(OBJ_FILES)) $(wildcard *.c.pre) ; do echo "  Deleting $$f"; rm $$f ; done
