/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * _tcpconstat_getsock.h: (Internal) module for SOL_TCP-based retrieval
 * of TCP connection infos.
 *
 *   Retrieve TCP connection infos via a `getsockopt(...)`.
 *
 * (part of TCPconstat)
 * Copyright	(C) 2014-2022 sedret452
 */

int tcpconstat_getsock (int);
