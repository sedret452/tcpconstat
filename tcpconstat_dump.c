/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * tcpconstat_dump.c: TCPconstat value dumping module, facility for dev
 * debugging.
 *
 *  Provide dev-specific dumping functions, for TCP and Netlink
 *  protocol data.
 *
 * (part of TCPconstat)
 * Copyright	(C) 2014-2022 sedret452
 */

#include "log_lib_levels.h"
#define  LOG_LEVEL_MOD  LOG_DEBUG
#include "log_lib.h"

#include <inttypes.h>
#include <linux/netlink.h>
#include <linux/inet_diag.h>

void
_dump_bytes (uint8_t* addr, uint32_t len, const char* prefix)
{
	uint32_t i = 0;
	uint16_t short_addr = ((uint16_t) addr);  // shortend to last two bytes

	log_debug ("%s        | 0x00 0x01 0x02 0x03 0x04 0x05 0x06 0x07\n", prefix);
	log_debug ("%s        | ---------------------------------------\n", prefix);

	log_debug ("%s 0x%04x | ", prefix, short_addr);
	for (; i < len; i++) {
		if (addr) {
			if (i && !((i+1) % 8)) {
				log_debug_cont ("0x%02x\n", *addr);
				log_debug ("%s 0x%04x | ", prefix, short_addr + ((i + 1) << 1));
			}
			else {
				log_debug_cont ("0x%02x ", *addr);
			}
		}
		else {
			break;
		}
		addr++;
	}
	log_debug_cont ("\n");

}

void
_dump_nl_header (struct nlmsghdr* nl_header)
{
	log_debug ("== Netlink Header ==\n");
	_dump_bytes ((uint8_t*) nl_header, NLMSG_HDRLEN ,"  ");
}

void
_dump_inet_diag_msg (struct inet_diag_msg* diag_msg)
{
	log_debug ("== Netlink Payload (inet_diag_msg) ==\n");
	_dump_bytes ((uint8_t*) diag_msg, sizeof (struct inet_diag_msg), "  ");
}

void
_dump_inet_diag_req (struct inet_diag_req* diag_req)
{
	log_debug ("== inet_diag_req ==\n");
	_dump_bytes ((uint8_t*) diag_req, sizeof (struct inet_diag_req), "  ");
}
