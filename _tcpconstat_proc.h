/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * _tcpconstat_proc.h: (Internal) module for /proc-based retrieval of TCP
 * connection infos.
 *
 *   Retrieve TCP connection infos via /proc/net/netlink.
 *
 * (part of TCPconstat)
 * Copyright	(C) 2014-2022 sedret452
 */

int tcpconstat_proc (void);
