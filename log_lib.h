/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * log_lib.h: Logging library for compile-time configurable logging
 * mechanism, similar to log4j.
 *
 * log_lib_levels.h  -- Definition of logging levels
 *
 * log_lib_default.h -- User configurable default configurations
 * (unique to each program)
 *
 * To prepare a module for including this functionality use following:
 *
 * Module 1
 *  #include "log_lib_levels.h"  // --> defines LOG_DEBUG, LOG_INFO, ...
 *  #define  LOG_LEVEL_MOD  LOG_INFO
 *  #include "log_lib.h"
 *
 * Module 2
 *  #include "log_lib_levels.h"
 *  #define  LOG_LEVEL_MOD  LOG_DEBUG
 *  #include "log_lib.h"
 *
 * Module 3 (no special logging level needed here)
 *  #include "log_lib.h"
 *
 * Defining defaults in "log_lib_default.h" for all the program
 *  #include "log_lib_levels.h"
 *  #define  LOG_LEVEL_DEFAULT LOG_WARN
 *
 * Overwright everything of the above by using FORCE
 *  #define  LOG_LEVEL_DEFAULT_FORCE  LOG_DEBUG
 *
 * (part of TCPconstat)
 * Copyright	(C) 2014-2022 sedret452
 */

#ifndef __LOG_LIB_H_
#define __LOG_LIB_H_

/* TODO
 *
 * How to handle multi-line log messages?  Should each line be
 * prepended using:
 *
 * DBG: line 1
 * DBG:    something on line 2
 * DBG:    something on line 3
*/

#include "log_lib_levels.h"
#include "log_lib_default.h"

/* LOG_LEVEL_DEFAULT_FORCE
 *
 * Overwriting LOG_LEVEL for *all*  modules.
 */
#ifdef LOG_LEVEL_DEFAULT_FORCE
#define LOG_LEVEL LOG_LEVEL_DEFAULT_FORCE
#else
/* LOG_LEVEL_MOD
 *
 * Define LOG_LEVEL for an individual module.
 */
#ifdef  LOG_LEVEL_MOD
#define LOG_LEVEL LOG_LEVEL_MOD
#endif 
#endif // LOG_LEVEL_DEFAULT_FORCE

/* LOG_LEVEL_DEFAULT
 *
 * Fallback rule, if LOG_LEVEL wasn't defined until here!
 */
#ifndef LOG_LEVEL
#define LOG_LEVEL LOG_LEVEL_DEFAULT
#endif

#include <stdio.h>

#if LOG_LEVEL >= LOG_DEBUG
#define log_debug(...) printf ("DBG: " __VA_ARGS__)
#define log_debug_cont(...) printf (__VA_ARGS__)
#else
#define log_debug(...)
#define log_debug_cont(...)
#endif

#if LOG_LEVEL >= LOG_INFO
#define log_info(...) printf ("INFO: " __VA_ARGS__)
#define log_info_cont(...) printf (__VA_ARGS__)
#else
#define log_info(...)
#define log_info_cont(...)
#endif

#if LOG_LEVEL >= LOG_WARN
#define log_warn(...) printf ("WARN: " __VA_ARGS__)
#define log_warn_cont(...) printf (__VA_ARGS__)
#else
#define log_warn(...)
#define log_warn(...)
#endif

#define log(...) printf (__VA_ARGS__);

#endif /* __LOG_LIB_H_ */
