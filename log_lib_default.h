/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * log_lib_default.h: Main configuration of logging defaults.
 *
 *  Allows to define logging defaults, as baseline, which can be
 *  overwritten individually, per module.
 *
 *  Defining LOG_LEVEL_DEFAULT_FORCE, allows to define a logging
 *  default that overrules module individual configurations.
 *
 * (part of TCPconstat)
 * Copyright	(C) 2014-2022 sedret452
 */

#include "log_lib_levels.h"

#define LOG_LEVEL_DEFAULT  LOG_INFO

#define LOG_LEVEL_MOD_TCPCONSTAT_LOG   LOG_INFO_TCP_INFO

/* #define LOG_LEVEL_MOD_TCPCONSTAT_DUMP  LOG_DEBUG_DUMP */
//#define LOG_LEVEL_MOD_TCPCONSTAT_DUMP  LOG_INFO

/*
#define LOG_LEVEL_DEFAULT_FORCE LOG_DEBUG
*/

